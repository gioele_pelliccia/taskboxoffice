﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TaskBoxOffice.Models
{
    public partial class Film
    {
        public int FilmId { get; set; }
        public string Titolo { get; set; }
        public string Img { get; set; }
        public string Descrizione { get; set; }
        public string Regista { get; set; }
        public string Produttore { get; set; }
        public DateTime? DataUscita { get; set; }
        public int? Durata { get; set; }
        public long? Incasso { get; set; }
    }
}
